package sheridan;

import static org.junit.Assert.*;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	
	
	@Test
	public void testDrinksRegular() {
		MealsService meals = new MealsService();	
		assertNotEquals("", meals.getAvailableMealTypes(MealType.DRINKS).equals("") );
	}
	
	@Test
	public void testDrinkException() {
		MealsService meals = new MealsService();
		assertFalse("message on null incorrect", meals.getAvailableMealTypes(null).equals("No Brands Available"));

	}
	
	@Test 
	public void testDrinksBoundaryIn() {
		MealsService meals = new MealsService();
		assertNotEquals("", meals.getAvailableMealTypes(MealType.DRINKS).size() >3  );
	}

	@Test
	public void testDrinksBoundaryOut() {
		MealsService meals = new MealsService();
		assertNotEquals("", meals.getAvailableMealTypes(MealType.DRINKS).size() == 1);
	}
}
